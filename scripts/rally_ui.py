#!/usr/bin/env python
######################################################
# GUI control code for rally car
# This script creates the GUI thuogh which Rally car 
#   can be semi-autonomously controlled 
# The script uses parts of gui_example_students.py code
#   written by Dr. BC Min for creating the GUI
#
# Author         Version
#__________    ____________
# Mythra         25/4/18        mbalakun@purdue.edu
######################################################

# Dependencies
import argparse         # Parsing input port argument
import serial           # communicating with Roomba
import time             # For pause, sleep/delay 
import Tkinter as tk    # For creating the GUI
from Tkinter import *
import tkMessageBox
from PIL import Image   
import threading        # To creat threads
import numpy as np
from binascii import hexlify
import rospy
from std_msgs.msg import String
import math
from msgsrv.srv import waypt
import geometry_msgs.msg
from visualization_msgs.msg import Marker

class rally_guictrl():
    def __init__(self, port):        
    # Flags
        # Shutdown flag
        self.shtdwn = False      # To trigger shutdown procedures 
        self.serflag = False     # To connect serial or not
        self.wayptflag = False   # To indicate if waypts are selected
         
    #Serial init parameters        
        
        if self.serflag:
            self.ser = serial.Serial(port, 115200)
            self.ser.flush()
            self.ser.close()
            self.ser.open()
    # Gui stuff
        self.root = tk.Tk()
    

#######################################################
#################  Control function ###################
#######################################################
    def ctrl(self,state=-1):
        # Get the velocity set from GUI
        vel = self.vscale.get()
        # Clear canvas
        self.cnv.delete("all")
        
        if self.wayptflag or state == 2:
            # Stop
            if state == 0:
                self.cnv.create_oval(0, 0, 100, 100, width=0, fill='red')    
                self.clog.insert(0.0,'Stop \n')
                # Send command to stop
                # ................ Needs to be written
                
            elif state == 1:                    # Start/Continue
                # Draw triangle to indicate forward/ motion
                points = [50,0, 0,100, 100, 100]   # Set vertices
                self.cnv.create_polygon(points, fill='green', width=3)
                self.clog.insert(0.0,'Start \n'+'-'*30+'\n')
                # Send command to start/continue robot motion
                # ................ Needs to be written
                
            elif state == 2:                    # Load waypts
                # Update display and text
                self.cnv.create_oval(0,0,100,100, width=0, fill='yellow')
                self.clog.insert(0.0, 'Selecting waypts..... \n')
                
                # Get selected waypts
                # Implement checks to ensure complete set is provided
                #  if not proceed until selected waypt
                # ................ Needs to be written
                # Load the waypts
                self.waypts = self.getwaypts(1).mrkr.points
                #print('_'*50)
                #print(self.waypts[1].x)    # For debug
                self.wayptflag = True       # Set waypoints_loaded? flag to true
                self.clog.insert(0.0,'-'*30+'\n')                
                self.clog.insert(0.0,'Set car at starting waypt then click Run\n')
                
            elif state == 3:                    # PID tuning
                # Give options for Kp Kd tuning
                # Draw directional triangle
                points = [0,50,100,0, 100, 100]
                self.cnv.create_polygon(points, fill='yellow', width=3)    
                self.clog.insert(0.0,' PID Tuning..... \n')
        else:
                self.clog.insert(0.0,' Please select waypts first \n')

#######################################################
#### function which creates GUI create and runs #######
#######################################################             
    def gui(self):
        # Make Gui window, title and background
        print('Setting up GUI')
        self.root.title("Rrrrally Gui - @Mythra")
        self.root.config(background = "#FFFFFF")
        
        rightFrame = tk.Frame(self.root, width=200, height = 600)
        rightFrame.grid(row=0, column=1, padx=10, pady=2)
        
        self.cnv = tk.Canvas(rightFrame, width=100, height=100, bg='white')
        self.cnv.grid(row=0, column=0, padx=10, pady=2)
        
        btnFrame = tk.Frame(rightFrame, width=200, height = 200)
        btnFrame.grid(row=1, column=0, padx=10, pady=2)
        
        # Create slider for velocity values
        self.vscale = Scale(btnFrame, from_=0, to=100, orient = HORIZONTAL)
        self.vscale.set(50) # Default speed
        self.vscale.grid(row=2, column=0, padx=10, pady=10)
        # Set label for above slider
        vlbl = Label(btnFrame, text = 'Max Speed(0-100%)')
        vlbl.grid(row=3, column=0, padx=20, pady=1)
        
        self.clog = tk.Text(rightFrame, width = 30, height = 10, takefocus=0)
        self.clog.grid(row=2, column=0, padx=10, pady=2)

        moveForwardBtn = tk.Button(btnFrame, text="Run", command=lambda: self.ctrl(1))
        moveForwardBtn.grid(row=0, column=1, padx=10, pady=2)
        
        moveLeftBtn = tk.Button(btnFrame, text="Load Waypts", command=lambda: self.ctrl(2))
        moveLeftBtn.grid(row=1, column=0, padx=10, pady=2)
        
        moveRightBtn = tk.Button(btnFrame, text="Tune PID", command= lambda: self.ctrl(3))
        moveRightBtn.grid(row=1, column=2, padx=10, pady=2)
        
        moveBackwardBtn = tk.Button(btnFrame, text="Stop", command= lambda: self.ctrl(0))
        moveBackwardBtn.grid(row=2, column=1, padx=10, pady=2)

        #stopBtn = tk.Button(btnFrame, text="Stop", command= lambda: self.ctrl(0))
        #stopBtn.grid(row=1, column=1, padx=10, pady=2)

        # Create init text and images
        self.cnv.create_oval(0,0,100,100,width=0,fill='red')
        self.clog.insert(0.0,'Select Waypts on Rviz to start (use \'g\')\n')
        self.clog.insert(0.0,'Click load waypts after select\n')


###################################################################
# Termination function, closes ports and kills all threads  
    def shutdown(self):
        if tkMessageBox.askokcancel("Quit", "Do you want to quit?"):
            print('Shutting down ... Thanks!')
            # Set shtdwn flags
            self.shtdwn = True   
            # Kill all threads
            self.root.destroy()
                        
# Main function
    def main(self):
    # initialize ROS
        rospy.init_node('rally_gui', anonymous = True)
        rospy.wait_for_service('get_waypt')
        self.getwaypts = rospy.ServiceProxy('get_waypt',waypt)
        # Create gui
        self.gui()
        # Create thread to read sensors
        #sensethrd = threading.Thread(None, self.readSensors)
        #sensethrd.start()
        # Check for window close
        self.root.protocol("WM_DELETE_WINDOW", self.shutdown) 
        # Start GUI
        self.root.mainloop()

########################################################################                
# The start .......              
if __name__ == "__main__":
    print('____________ RALLY-CAR GUI CONTROL _____________')
    # Parse input arguments
    # To check if port was specified
    parser = argparse.ArgumentParser(
        description='''Script to control rally car using GUI.
            Please quit by closing the GUI window (x on GUI), this ensures the code executes proper termination hooks( closing ports, stop OI robot,  kill infinite loops, etc). \n Check optional parameters to run below ''')
    parser.add_argument('--port', default = '/dev/ttyUSB0',
            help = 'Port name (provide if it is NOT %(default)s,  Ex: python rally_ui.py --port \'/dev/ttyUSB1\' or python rally_ui.py --port \'COM1\'')
    parser.add_argument('name')
    parser.add_argument('log')
    args = parser.parse_args()
    print('Setting port to ' + args.port)   
    # Create rally_car_ui object
    rcar = rally_guictrl(args.port)   
    # Start the guicontrol
    rcar.main() 
